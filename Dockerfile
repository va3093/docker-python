FROM docker:latest

RUN apk add --update libpq-dev
RUN apk add --update python3

RUN apk update \
  && apk add --virtual build-deps gcc python3-dev musl-dev \
  && apk add postgresql-dev \
  && pip install psycopg2 \
  && apk del build-deps \
  && apk add gcc \
  && apk add libc-dev \
  && apk add linux-headers
